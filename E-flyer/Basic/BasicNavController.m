//
//  BasicNavController.m
//  E-flyer
//
//  Created by 张杰 on 16/5/18.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import "BasicNavController.h"

@interface BasicNavController ()

@end

@implementation BasicNavController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self settingTabBarSelectedImg];
}
//设置tabbar 图片选中的图片渲染模式
- (void)settingTabBarSelectedImg{
    self.tabBarItem.selectedImage = [self.tabBarItem.selectedImage imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
}

@end
