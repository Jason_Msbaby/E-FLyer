//
//  EFTabBarItem.m
//  E-flyer
//
//  Created by 张杰 on 16/5/18.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import "EFTabBarItem.h"
IB_DESIGNABLE
@implementation EFTabBarItem


- (void)setMySelectedImage:(UIImage *)mySelectedImage{
    self.selectedImage = [mySelectedImage imageWithRenderingMode:(UIImageRenderingModeAlwaysOriginal)];
}



@end
