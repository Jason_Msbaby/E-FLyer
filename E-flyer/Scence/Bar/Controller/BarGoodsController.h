//
//  BarGoodsController.h
//  E-flyer
//
//  Created by 张杰 on 16/5/10.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import "BasicController.h"
@class EFUser;
@interface BarGoodsController : BasicController
@property(nonatomic,strong) EFUser *barUser;
@end
