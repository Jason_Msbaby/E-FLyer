//
//  BarListCell.h
//  E-flyer
//
//  Created by 张杰 on 16/5/3.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "EFUser.h"
#import <UIKit/UIKit.h>

@interface BarListCell : UICollectionViewCell
@property(nonatomic,strong) EFUser *model;
@end
