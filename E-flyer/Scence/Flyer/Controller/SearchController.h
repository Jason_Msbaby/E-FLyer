//
//  SearchController.h
//  E-flyer
//
//  Created by Jason_Msbaby on 16/2/27.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import "BasicController.h"

@interface SearchController : BasicController
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@end
