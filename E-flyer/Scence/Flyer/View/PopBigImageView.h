//
//  PopBigImageView.h
//  E-flyer
//  点击图片弹出大图
//  Created by 苗爽 on 16/4/22.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PopBigImageView : UIView

- (instancetype)initWithImage:(UIImage *)img;

- (void)pop;

@end
