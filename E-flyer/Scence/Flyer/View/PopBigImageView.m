//
//  PopBigImageView.m
//  E-flyer
//
//  Created by 苗爽 on 16/4/22.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "UIView+EFView.h"
#import "PopBigImageView.h"

@interface PopBigImageView ()
@property(nonatomic,strong) UIScrollView  *scrollView;
@property(nonatomic,strong) UIImageView *imgView;
@end

@implementation PopBigImageView

- (instancetype)initWithImage:(UIImage *)img{
    if (self = [super initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)]) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
        self.scrollView = [UIScrollView new];
        //滚动自适应
        [self addSubview:self.scrollView];
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self).with.insets(UIEdgeInsetsMake(0,0,0,0));
        }];
        UIView *container = [UIView new];
        [self.scrollView addSubview:container];
        [container mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.scrollView);
            make.width.equalTo(self.scrollView);
        }];
        
        
        UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 1000)];
        [container addSubview:v];
//        v.backgroundColor = kRandomColor;
        
        
        
        CGFloat imgHeight = [self heightForImage:img];
        
        
        self.imgView = [[UIImageView alloc] initWithImage:img];
        [container addSubview:self.imgView];
        [self.imgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(container);
            make.height.equalTo(@(imgHeight));
            if (imgHeight > kScreenHeight) {
                make.top.equalTo(container);
            }else{
                make.centerY.equalTo(self);
            }
        }];
        
        self.imgView.contentMode = UIViewContentModeScaleToFill;
        self.imgView.userInteractionEnabled = YES;
        [self.scrollView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)]];
        
        
        
        [container mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self.imgView.mas_bottom);
        }];
        
        
    }
    return self;
}


- (CGFloat)heightForImage:(UIImage *)image{
    CGSize imgSize = image.size;
    CGFloat scale = imgSize.width/kScreenWidth;
    CGFloat imgHeight = imgSize.height/scale;
    return imgHeight;
}

- (void)pop{
//    [UIView animateWithDuration:1.5 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
//    }];
}

- (void)tap{
    [self removeFromSuperview];
    self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0];
}
@end
