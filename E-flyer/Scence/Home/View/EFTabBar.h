//
//  EFTabBar.h
//  E-flyer
//
//  Created by Jason_Msbaby on 16/2/26.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "BasicModel.h"
#import <UIKit/UIKit.h>

@interface EFTabBar : UITabBar
@property (nonatomic, weak) UIButton *addButton;
@end
