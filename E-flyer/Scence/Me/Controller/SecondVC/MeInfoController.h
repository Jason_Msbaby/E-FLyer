//
//  MeInfoController.h
//  E-flyer
//  个人中心信心展示通用controller
//  Created by Jason_Msbaby on 16/4/17.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "MeMenu.h"
#import "BasicController.h"

@interface MeInfoController : BasicController
@property(strong,nonatomic) MeMenu *menu;
@end
