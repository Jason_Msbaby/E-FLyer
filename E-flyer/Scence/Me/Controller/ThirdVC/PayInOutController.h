//
//  PayInOutController.h
//  E-flyer
//  充值提现页面
//  Created by 苗爽 on 16/4/25.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "EFLog.h"
#import "BasicController.h"

@interface PayInOutController : BasicController
@property(nonatomic,assign)EFLogType  type;//充值还是提现
@end
