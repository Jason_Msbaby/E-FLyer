//
//  AcountRecordCell.h
//  E-flyer
//
//  Created by 苗爽 on 16/4/23.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//
#import "EFLog.h"
#import <UIKit/UIKit.h>

@interface AcountRecordCell : UITableViewCell
@property(nonatomic,strong) EFLog *model;
@end
