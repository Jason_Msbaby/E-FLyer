//
//  UserRecordCell.h
//  E-flyer
//
//  Created by 苗爽 on 16/4/23.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFReciveOrder.h"
@interface UserRecordCell : UITableViewCell
@property(nonatomic,strong) EFReciveOrder *model;
@end
