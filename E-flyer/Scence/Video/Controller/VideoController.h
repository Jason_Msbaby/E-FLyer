//
//  VideoController.h
//  E-flyer
//  短片视图控制器
//  Created by Jason_Msbaby on 16/3/2.
//  Copyright © 2016年 Jason_Msbaby. All rights reserved.
//

#import "BasicController.h"

@interface VideoController : BasicController
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
